
import requests
import argparse
import json
import logging as log
import time


def parse_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-u', '--url', required=True, help="connector url")

    parser.add_argument('-f', '--config-file', required=True, help="connector config file")

    parser.add_argument('-n', '--connector-name', required=True, help="connector name")

    parser.add_argument('-r', '--response-code', required=True, help="expected response code")

    parser.add_argument('-i', '--retry-interval', required=True, help="retry interval in seconds")

    return parser.parse_args()

HEADERS = {"Content-Type": "application/json"}
NAME_FIELD = 'name'
CONFIG_FIELD = 'config'

class Configurator:
    def __init__(self, args):
        self.url = args.url
        self.config_file_path = args.config_file
        self.connector_name = args.connector_name
        self.expected_response_code = int(args.response_code)
        self.retry_interval = int(args.retry_interval)

        log.info("url: '{0}'".format(self.url))
        log.info("config_file_path: '{0}'".format(self.config_file_path))
        log.info("connector_name: '{0}'".format(self.connector_name))
        log.info("expected_response_code: '{0}'".format(self.expected_response_code))
        log.info("retry_interval: '{0}'".format(self.retry_interval))

        self.config = self.load_config()
        self.config[NAME_FIELD] = self.connector_name
        log.info("config: '{0}'".format(self.config))

        self.expected_config = self.stringify_fields_in_dict(self.config)
        log.info("expected_config: '{0}'".format(self.expected_config))

    def run(self):

        while True:
            log.info("Trying to configure.")
            try:
                response = requests.put(self.url, json=self.config)
            except Exception as e:
                log.info("Couldn't connect to host '{0}'".format(e))
                self.sleep()
                continue

            try:
                response_config = json.loads(response.text)[CONFIG_FIELD]
            except Exception as e:
                log.error("Couldn't parse response '{0}', exception '{1}'".format(response.text, e))
                self.sleep()
                continue

            if response.status_code != self.expected_response_code:
                log.info("Got response code {0}, expected was {1}.".format(response.status_code, self.expected_response_code))
                self.sleep()
                continue

            if response_config != self.expected_config:
                log.info("Got response text '{0}', expected was '{1}'.".format(response_config, self.expected_config))
                self.sleep()
                continue
            
            log.info("Configuration successful.")
            break

    def sleep(self):
        log.info("Wait {0} seconds.".format(self.retry_interval))
        time.sleep(self.retry_interval)

    def load_config(self):
        with open(self.config_file_path, 'r') as json_file:
            return json.load(json_file)

    @staticmethod
    def stringify_fields_in_dict(input_dict):
        result_dict = {}

        for key, value in input_dict.items():
            if value == True:
                result_dict[key] = 'true'
            elif value == False:
                result_dict[key] = 'false'
            else:
                result_dict[key] = value

        return result_dict

def main():
    log.basicConfig(level=log.INFO)

    args = parse_args()

    configurator = Configurator(args)
    configurator.run()

if __name__ == "__main__":
    main()

# Docker registry

The following setup is tested on macOS and Linux.

## Setup steps

```sh
mkdir data
mkdir auth
cd auth
htpasswd -Bc registry.password admin
# provide a new password
```

Run docker-compose.
```sh
cd ..
docker-compose up -d
```

Check if possible to log in.
```sh
docker login localhost:5005 -u admin -p <password>
```

In kubernetes a secret is needed to be set up to be able to pull images from the registry. Here we create a secret named "regcred" which could be used by the deployments.
```sh
kubectl create secret docker-registry regcred --docker-server=localhost:5005 --docker-username=admin --docker-password=<password>
kubectl get secrets
```

## Register images
### Example:

Build an image.
```sh
# first navigate to the folder containing your Dockerfile
docker build -t <image-name> .
```

Tag and push it.
```sh
docker tag <image-name> localhost:5005/<image-name>
docker push localhost:5005/<image-name>
```

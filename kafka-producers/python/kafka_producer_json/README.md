# Simple kafka producer with json encoding

## Description

The script sends user defined json messages to kafka.

## Usage

### Examples
```sh
python kafka_producer_json.py -h

python kafka_producer_json.py -b 'localhost:9092' -t user -m '{"id": 1001, "email": "user@test.com"}'

python kafka_producer_json.py -b 'localhost:9092' -t user -m '{"schema": {"type": "struct", "optional": false, "version": 1, "fields": [{ "field": "id", "type": "string", "optional": true }, { "field": "email", "type": "string", "optional": true}]}, "payload": {"id": "1001","email": "user@test.com"}}' -n 3

python kafka_producer_json.py -b 'localhost:9092' -t user -m '{"schema": {"type": "struct", "optional": false, "version": 1, "fields": [{ "field": "id", "type": "string", "optional": true }, { "field": "email", "type": "string", "optional": true}]}, "payload": {"id": "1001","email": "user@test.com"}}' -c

python kafka_producer_json.py -b 'localhost:9092' -t user -m '{"id": 1001, "email": "user@test.com"}' -n 3

python kafka_producer_json.py -b 'localhost:9092' -t user -m \
'{
    "id": 1001,
    "email": "user@test.com"
}'
```

### Python dependencies

kafka-python lib:
```sh
pip install kafka-python
```

Tested with Python 3.9.1

## Kubernetes deployment

```sh
docker image build . -t kafka-producer-json
docker tag kafka-producer-json localhost:5005/kafka-producer-json
docker push localhost:5005/kafka-producer-json
```

In case not using the local Docker Registry edit the "image:" and add the path of your image.
Create the pod:
```sh
kubectl apply -f kafka_producer_pod.yaml
```

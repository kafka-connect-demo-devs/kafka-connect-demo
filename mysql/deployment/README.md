# Kubernetes deployment

**Note: this deployment doesn't deploy the mysql-autoconf from the above mysql root directory. This deploys Mysql from the original image and the configuration is need to be done manually as described below.**

## Deploy
```sh
./deploy_mysql.yaml
```

## Configure
```sh
# exec into mysql pod
kubectl exec -it <mysql-pod> -- sh
# log into mysql
mysql -uroot -p
# configure mysql
CREATE USER 'testuser'@'localhost' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON *.* TO 'testuser'@'localhost' WITH GRANT OPTION;
CREATE USER 'testuser'@'%' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON *.* TO 'testuser'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
quit
# exit from pod's shell
exit
```

## Undeploy
```sh
./undeploy_mysql.yaml
```

Useful commands:

```sh
# create service
kubectl create -f <service-yaml-file>
# list services
kubectl get services
# delete service
kubectl delete service <service-name>
```

```sh
# create deployment
kubectl create -f <deployment-yaml-file>
# list deployments
kubectl get deployments
# delete deployment
kubectl delete deployment <deployment-name>
```

Port forward to be able to access services locally
```sh
kubectl port-forward <pod> <local-port>:<port-of-pod>
# example
kubectl port-forward mysql-84b5ff985-rxv28 3306:3306
# now Mysql service could be accessed on localhost:3306
```
